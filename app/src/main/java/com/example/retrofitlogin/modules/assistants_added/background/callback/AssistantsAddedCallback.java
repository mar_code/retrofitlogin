package com.example.retrofitlogin.modules.assistants_added.background.callback;

public interface AssistantsAddedCallback {
    void onSuccessAddAssistant();
}
