package com.example.retrofitlogin.modules.assistants_added;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.assistants.AssistantsActivity;
import com.example.retrofitlogin.modules.assistants.background.adapter.AssistantsAdapter;
import com.example.retrofitlogin.modules.assistants.background.presenter.AssistantsPresenter;
import com.example.retrofitlogin.modules.assistants_added.background.callback.AssistantsAddedCallback;
import com.example.retrofitlogin.modules.assistants_added.background.presenter.AssistantsAddedPresenter;

public class AssistantsAddedActivity extends AppCompatActivity implements AssistantsAddedCallback {

    private AssistantsAddedPresenter mAssistantsAddedPresenter;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, AssistantsAddedActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistants_added);

        mAssistantsAddedPresenter = new AssistantsAddedPresenter(this,this);
        mAssistantsAddedPresenter.doAddedAssistant();
    }

    @Override
    public void onSuccessAddAssistant() {
        Log.d("TEST_AUX", "Exito");
    }
}