package com.example.retrofitlogin.modules.login.background.callback;

import com.example.retrofitlogin.modules.login.background.response.LoginResponse;

public interface LoginCallback {
    void onSuccessLogin(LoginResponse loginResponse);
}