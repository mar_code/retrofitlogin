package com.example.retrofitlogin.modules.assistants.background.request;

import com.example.retrofitlogin.background.webservices.request.BaseRequest;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AssistantsRequest extends BaseRequest implements Serializable {

    @SerializedName("IdOperario")
    public String IdOperator;

    public AssistantsRequest(String idOperator) {
        IdOperator = idOperator;
    }
}
