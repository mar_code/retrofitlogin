package com.example.retrofitlogin.modules.assistants.background.response;

import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import io.realm.RealmList;
import io.realm.RealmObject;

public class AssistantsResponse implements Serializable {
    @SerializedName("result")
    public String result;
    @SerializedName("resultDescription")
    public String resultDescription;
    @SerializedName("Auxiliares")
    public RealmList<Assistant> assistant;
}
