package com.example.retrofitlogin.modules.assistants_added.background.request;

import com.google.gson.annotations.SerializedName;

public class AssistantsAddedRequestModel {
    @SerializedName("IdOTFFM")
    public String workOrderId;
    @SerializedName("IdAuxiliar")
    public String auxId;
    @SerializedName("IdOperario")
    public String operatorId;

    public AssistantsAddedRequestModel(String workOrderId, String auxId, String operatorId) {
        this.workOrderId = workOrderId;
        this.auxId = auxId;
        this.operatorId = operatorId;
    }
}
