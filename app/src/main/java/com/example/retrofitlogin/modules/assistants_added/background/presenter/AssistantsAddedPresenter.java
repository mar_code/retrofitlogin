package com.example.retrofitlogin.modules.assistants_added.background.presenter;

import android.os.AsyncTask;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.retrofitlogin.background.webservices.WebService;
import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;
import com.example.retrofitlogin.modules.assistants_added.background.asynctask.AddAssistant;
import com.example.retrofitlogin.modules.assistants_added.background.callback.AssistantsAddedCallback;
import com.example.retrofitlogin.modules.assistants_added.background.request.AssistantsAddedRequest;
import com.example.retrofitlogin.modules.assistants_added.background.request.AssistantsAddedRequestModel;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;

import java.io.IOException;
import java.util.ArrayList;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssistantsAddedPresenter {

    private AssistantsAddedCallback mAssistantsAddedCallback;
    private AppCompatActivity mAppCompatActivity;
    private String mIdAux;
    private String mIdOperator;
    private AssistantsAddedRequestModel mAssistantsAddedRequestModel;
    private ArrayList<AssistantsAddedRequestModel> mListAssistantModel;

    public AssistantsAddedPresenter(AssistantsAddedCallback mAssistantsAddedCallback, AppCompatActivity mAppCompatActivity) {
        this.mAssistantsAddedCallback = mAssistantsAddedCallback;
        this.mAppCompatActivity = mAppCompatActivity;
    }

    public void doAddedAssistant()
    {
        mListAssistantModel = new ArrayList<>();

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Assistant assistant = realm.where(Assistant.class).findFirst();
        LoginResponse loginResponse = realm.where(LoginResponse.class).findFirst();
        realm.copyToRealmOrUpdate(loginResponse);
        realm.copyToRealmOrUpdate(assistant);
        mIdAux = assistant.idAssistant;
        mIdOperator = loginResponse.idOperator;
        realm.commitTransaction();
        realm.close();

        Toast.makeText(mAppCompatActivity, ""+assistant.name, Toast.LENGTH_SHORT).show();

        mAssistantsAddedRequestModel = new AssistantsAddedRequestModel("", mIdAux, mIdOperator);
        mListAssistantModel.add(mAssistantsAddedRequestModel);

        AssistantsAddedRequest mAssistantsAddedRequest = new AssistantsAddedRequest(mListAssistantModel);
        Call<AssistantsAddedRequest> call = WebService.services().addAssistant(mAssistantsAddedRequest);

        AddAssistant addAssistant =  new AddAssistant(mAssistantsAddedRequest);

        addAssistant.execute();
    }

}
