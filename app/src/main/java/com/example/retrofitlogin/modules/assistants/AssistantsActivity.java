package com.example.retrofitlogin.modules.assistants;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.assistants.background.adapter.AssistantsAdapter;
import com.example.retrofitlogin.modules.assistants.background.callback.AssistantsCallback;
import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;
import com.example.retrofitlogin.modules.assistants.background.presenter.AssistantsPresenter;

import java.util.ArrayList;

import io.realm.RealmList;

public class AssistantsActivity extends AppCompatActivity implements AssistantsCallback {
    private AssistantsPresenter mAssistantsPresenter;

    private ArrayList<Assistant> mAssistantList;
    private RecyclerView mAssistantRecycler;
    private AssistantsAdapter mAssistantAdapter;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, AssistantsActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assistants);

        mAssistantList = new ArrayList<>();

        mAssistantRecycler = findViewById(R.id.act_assistants_recycler);
        LinearLayoutManager lm = new LinearLayoutManager(this);
        mAssistantRecycler.setLayoutManager(lm);
        mAssistantRecycler.setHasFixedSize(true);

        mAssistantsPresenter = new AssistantsPresenter(this,this);
        mAssistantsPresenter.doLoadAssistants();
    }

    @Override
    public void onSuccessLoadedAssistants(RealmList<Assistant> assistants) {
        mAssistantList.addAll(assistants);
        mAssistantAdapter = new AssistantsAdapter(this, mAssistantList);
        mAssistantRecycler.setAdapter(mAssistantAdapter);
    }
}