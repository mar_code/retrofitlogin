package com.example.retrofitlogin.modules.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.login.background.callback.LoginCallback;
import com.example.retrofitlogin.modules.login.background.presenter.LoginPresenter;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;
import com.example.retrofitlogin.modules.principal.PrincipalActivity;
import com.example.retrofitlogin.view.utils.MessageUtils;

public class LoginActivity extends AppCompatActivity implements LoginCallback {

    private LoginPresenter mLoginPresenter;

    private EditText mUserEditText, mPasswordEditText;
    private Button mLoginButton;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, LoginActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUserEditText = findViewById(R.id.act_login_user_edit_text);
        mPasswordEditText  = findViewById(R.id.act_login_password_edit_text);
        mLoginButton = findViewById(R.id.act_login_button_continue);

        mLoginButton.setOnClickListener(mLoginOnClickListener);

        mLoginPresenter = new LoginPresenter(this,this);
    }

    private View.OnClickListener mLoginOnClickListener = v -> {
        if (!mUserEditText.getText().toString().isEmpty() && !mPasswordEditText.getText().toString().isEmpty()) {
            mLoginPresenter.doLogin(mUserEditText.getText().toString(), mPasswordEditText.getText().toString());
        } else {
            Toast.makeText(this, "Valide los campos obligatorios.", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    public void onSuccessLogin(LoginResponse loginResponse) {
        MessageUtils.stopProgress();
        PrincipalActivity.launch(LoginActivity.this);
        LoginActivity.this.finish();
    }
}