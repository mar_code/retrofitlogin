package com.example.retrofitlogin.modules.principal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.assistants.AssistantsActivity;
import com.example.retrofitlogin.modules.login.LoginActivity;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;

import io.realm.Realm;

public class PrincipalActivity extends AppCompatActivity {

    private TextView mNameTextview, mExitTextView;
    private Button mContinueButton;
    private LoginResponse loginResponse;
    private Realm realm;

    public static void launch(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, PrincipalActivity.class);
        appCompatActivity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        mNameTextview = findViewById(R.id.act_principal_name_text_view);
        mExitTextView = findViewById(R.id.act_principal_exit_text_view);
        mContinueButton = findViewById(R.id.act_principal_continue_button);

        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        loginResponse = realm.where(LoginResponse.class).findFirst();
        realm.commitTransaction();

        mNameTextview.setText(loginResponse.userName);

        mExitTextView.setOnClickListener(mExitOnClickListener);
        mContinueButton.setOnClickListener(mContinueOnClickListener);
    }

    private View.OnClickListener mExitOnClickListener = v -> {
        if (loginResponse != null)
        {
            realm.beginTransaction();
            realm.deleteAll();
            realm.commitTransaction();
            realm.close();
            Log.d("MENSAJE", "EXIT");
        }
        LoginActivity.launch(PrincipalActivity.this);
        PrincipalActivity.this.finish();
    };

    private View.OnClickListener mContinueOnClickListener = v -> {
        AssistantsActivity.launch(PrincipalActivity.this);
        //PrincipalActivity.this.finish();
    };
}