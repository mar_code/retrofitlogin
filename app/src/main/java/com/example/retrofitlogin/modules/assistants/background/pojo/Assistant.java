package com.example.retrofitlogin.modules.assistants.background.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Assistant extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("IdAuxiliar")
    public String idAssistant;

    @SerializedName("nombre")
    public String name;
    @SerializedName("tipoAux")
    public String typeAssistant;
    @SerializedName("fotoAux")
    public String photoAssistant;

    public Assistant() {
        this.idAssistant = "";
        this.name = "";
        this.typeAssistant = "";
        this.photoAssistant = "";
    }

    public Assistant(String name) {
        this.name = name;
        this.idAssistant = "";
        this.typeAssistant = "";
        this.photoAssistant = "";
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
