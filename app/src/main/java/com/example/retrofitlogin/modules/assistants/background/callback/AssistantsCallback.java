package com.example.retrofitlogin.modules.assistants.background.callback;

import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;

import io.realm.RealmList;

public interface AssistantsCallback {
    void onSuccessLoadedAssistants(RealmList<Assistant> assistants);
}
