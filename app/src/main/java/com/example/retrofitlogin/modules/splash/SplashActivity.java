package com.example.retrofitlogin.modules.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.login.LoginActivity;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;
import com.example.retrofitlogin.modules.principal.PrincipalActivity;

import io.realm.Realm;

public class SplashActivity extends AppCompatActivity {

    private LoginResponse loginResponse;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();


        handler.postDelayed(new Runnable() {
            public void run() {
                realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                loginResponse = realm.where(LoginResponse.class).findFirst();
                realm.commitTransaction();

                if(loginResponse != null)
                {
                    PrincipalActivity.launch(SplashActivity.this);
                    SplashActivity.this.finish();
                }
                else{
                    LoginActivity.launch(SplashActivity.this);
                    SplashActivity.this.finish();
                }
            }
        }, 2200);
    }
}