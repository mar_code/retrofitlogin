package com.example.retrofitlogin.modules.login.background.presenter;

import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.retrofitlogin.background.webservices.WebService;
import com.example.retrofitlogin.modules.login.background.callback.LoginCallback;
import com.example.retrofitlogin.modules.login.background.request.LoginRequest;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;
import com.example.retrofitlogin.view.utils.MessageUtils;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter {

    private LoginCallback mLoginCallback;
    private AppCompatActivity mAppCompatActivity;

    public LoginPresenter(AppCompatActivity appCompatActivity, LoginCallback mLoginCallback) {
        this.mAppCompatActivity = appCompatActivity;
        this.mLoginCallback = mLoginCallback;
    }

    public void doLogin(String user, String password) {

        MessageUtils.progress(mAppCompatActivity, "Iniciando Sesión");

        LoginRequest loginRequest = new LoginRequest(user, password, "10.10.10.10", "");
        Call<LoginResponse> call = WebService.services().login(loginRequest);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if (response.isSuccessful()) {
                    LoginResponse loginResponse = response.body();

                    if (loginResponse.idResult.equals("0")) {

                        Realm realm = Realm.getDefaultInstance();

                        realm.beginTransaction();
                        realm.copyToRealmOrUpdate(loginResponse);
                        realm.commitTransaction();

                        realm.close();

                        mLoginCallback.onSuccessLogin(loginResponse);
                    } else {
                        Toast.makeText(mAppCompatActivity, loginResponse.resultDescription, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(mAppCompatActivity, "Error al consumir el servicio", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                //MessageUtils.stopProgress();
                Toast.makeText(mAppCompatActivity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}