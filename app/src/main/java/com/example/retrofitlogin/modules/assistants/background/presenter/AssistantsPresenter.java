package com.example.retrofitlogin.modules.assistants.background.presenter;

import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.retrofitlogin.background.webservices.WebService;
import com.example.retrofitlogin.modules.assistants.background.callback.AssistantsCallback;
import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;
import com.example.retrofitlogin.modules.assistants.background.request.AssistantsRequest;
import com.example.retrofitlogin.modules.assistants.background.response.AssistantsResponse;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AssistantsPresenter {

    private AssistantsCallback mAssistantsCallback;
    private AppCompatActivity mAppCompatActivity;

    public AssistantsPresenter(AppCompatActivity appCompatActivity, AssistantsCallback assistantsCallback) {
        this.mAppCompatActivity = appCompatActivity;
        this.mAssistantsCallback = assistantsCallback;
    }

    public void doLoadAssistants()
    {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        LoginResponse dataUserSaved = realm.copyFromRealm(realm.where(LoginResponse.class).findFirst());
        realm.commitTransaction();

        AssistantsRequest assistantsRequest = new AssistantsRequest(dataUserSaved.idOperator);
        Call<AssistantsResponse> call = WebService.services().assistants(assistantsRequest);

        call.enqueue(new Callback<AssistantsResponse>() {
            @Override
            public void onResponse(Call<AssistantsResponse> call, Response<AssistantsResponse> response) {
                if (response.isSuccessful()) {

                    AssistantsResponse assistantsResponse = response.body();

                    if(assistantsResponse.result.equals("0"))
                    {
                        RealmList<Assistant> assistants = assistantsResponse.assistant;

                        /*realm.beginTransaction();
                        realm.copyToRealmOrUpdate(assistants);
                        realm.commitTransaction();
                        Toast.makeText(mAppCompatActivity, ""+assistants.get(0).name, Toast.LENGTH_SHORT).show();

                        realm.close();*/

                        mAssistantsCallback.onSuccessLoadedAssistants(assistants);
                    }
                    else
                    {
                        Toast.makeText(mAppCompatActivity, assistantsResponse.resultDescription, Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(mAppCompatActivity, "Error al consumir el servicio", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AssistantsResponse> call, Throwable t) {
                Toast.makeText(mAppCompatActivity, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
