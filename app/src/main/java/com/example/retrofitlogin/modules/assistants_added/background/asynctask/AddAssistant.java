package com.example.retrofitlogin.modules.assistants_added.background.asynctask;

import android.os.AsyncTask;
import android.util.Log;

import com.example.retrofitlogin.background.webservices.WebService;
import com.example.retrofitlogin.modules.assistants_added.background.request.AssistantsAddedRequest;

import java.io.IOException;

import retrofit2.Call;

public class AddAssistant extends AsyncTask<Void, String, Boolean> {

    private AssistantsAddedRequest assistantsAddedRequest;

    public AddAssistant(AssistantsAddedRequest assistantsAddedRequest) {
        this.assistantsAddedRequest = assistantsAddedRequest;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        Call<AssistantsAddedRequest> call = WebService.services().addAssistant(assistantsAddedRequest);
        try {
            call.execute();
            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onPostExecute(Boolean res) {
        super.onPostExecute(res);
        if(res)
        {
            Log.d("TEST_LOG", "Operación Exitosa");
        }
        else
        {
            Log.d("TEST_LOG", "Falló la Operación");
        }
    }
}
