package com.example.retrofitlogin.modules.assistants.background.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.retrofitlogin.R;
import com.example.retrofitlogin.modules.assistants.background.pojo.Assistant;
import com.example.retrofitlogin.modules.assistants_added.AssistantsAddedActivity;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;

public class AssistantsAdapter extends RecyclerView.Adapter<AssistantsAdapter.ViewHolder> {

    private ArrayList<Assistant> mListAssistants;
    private AppCompatActivity mAppCompatActivity;
    private int position;
    Realm realm = Realm.getDefaultInstance();
    RealmList<Assistant> assistants;

    public AssistantsAdapter(AppCompatActivity appCompatActivity, ArrayList<Assistant> listAssistants) {
        this.mAppCompatActivity = appCompatActivity;
        this.mListAssistants = listAssistants;
    }

    @Override
    public AssistantsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_assistants,parent,false);
        return new AssistantsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AssistantsAdapter.ViewHolder holder, int position) {

        Assistant assistant = mListAssistants.get(position);
        holder.name.setText(assistant.name != null && !assistant.name.isEmpty() ? assistant.name : "");
        holder.itemView.setTag(assistant);
    }

    @Override
    public int getItemCount() {
        return mListAssistants.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);

            View.OnClickListener mOnClickListener = view -> {

                Assistant assistant1 = (Assistant) view.getTag();

                realm.beginTransaction();
                Assistant assistant_comprobation = realm.where(Assistant.class).findFirst();
                if(assistant_comprobation != null)
                {
                    realm.delete(Assistant.class);
                }
                realm.copyToRealmOrUpdate(assistant1);
                realm.commitTransaction();
                realm.close();

                AssistantsAddedActivity.launch(mAppCompatActivity);
            };

            itemView.setOnClickListener(mOnClickListener);
            name = itemView.findViewById(R.id.item_assistants_name);
        }

    }
}