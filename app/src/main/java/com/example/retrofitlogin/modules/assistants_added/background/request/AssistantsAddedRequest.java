package com.example.retrofitlogin.modules.assistants_added.background.request;

import com.example.retrofitlogin.background.webservices.request.BaseRequest;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AssistantsAddedRequest extends BaseRequest {
    @SerializedName("auxiliares")
    public ArrayList<AssistantsAddedRequestModel> assistants;

    public AssistantsAddedRequest(ArrayList<AssistantsAddedRequestModel> assistants) {
        this.assistants = assistants;
    }
}
