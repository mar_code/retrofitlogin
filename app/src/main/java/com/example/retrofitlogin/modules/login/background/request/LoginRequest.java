package com.example.retrofitlogin.modules.login.background.request;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginRequest implements Serializable {

    @SerializedName("usuario")
    public String usuario;
    @SerializedName("pass")
    public String mSecretWord;
    @SerializedName("password")
    public String mSecretWord2;
    @SerializedName("user")
    public String user;
    @SerializedName("userId")
    public String userId;
    @SerializedName("ip")
    public String ip;
    @SerializedName("deviceId")
    public String deviceId;

    @SerializedName("User")
    public final String user_2;
    @SerializedName("UserId")
    public final String user_3;
    @SerializedName("Password")
    public final String mSerializedSecretWord;
    @SerializedName("Ip")
    public final String ip_2;
    @SerializedName("IP")
    public final String ip_4;
    @SerializedName("direccionIp")
    public final String ip_5;

    public LoginRequest() {
        String userId = "25631";
        String secretWord = "Middle100$";
        String ip = "10.216.48.43";
        String deviceId = "fcmToken";
        this.userId = userId;
        this.user = userId;
        this.mSecretWord2 = secretWord;
        this.ip = ip;
        this.user_2 = userId;
        this.user_3 = userId;
        this.mSerializedSecretWord = secretWord;
        this.ip_2 = ip;
        this.deviceId = deviceId;
        this.usuario = userId;
        this.mSecretWord = secretWord;
        this.ip_4 = ip;
        this.ip_5 = ip;
    }

    public LoginRequest(String userId, String secretWord, String ip, String deviceId) {
        this.userId = userId;
        this.user = userId;
        this.usuario = userId;
        this.mSecretWord2 = secretWord;
        this.mSecretWord = secretWord;
        this.ip = ip;
        this.user_2 = userId;
        this.user_3 = userId;
        this.mSerializedSecretWord = secretWord;
        this.ip_2 = ip;
        this.deviceId = deviceId;
        this.ip_4 = ip;
        this.ip_5 = ip;
    }
}