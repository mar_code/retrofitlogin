package com.example.retrofitlogin.modules.login.background.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class LoginResponse extends RealmObject implements Serializable {

    @PrimaryKey
    @SerializedName("IdOperario")
    public String idOperator;

    @SerializedName("IdResult")
    public String idResult;
    @SerializedName("Result")
    public String result;
    @SerializedName("ResultDescription")
    public String resultDescription;
    @SerializedName("Path")
    public String path;
    @SerializedName("URLFoto")
    public String urlPhoto;
    @SerializedName("HoraEntrada")
    public String inHour;
    @SerializedName("HoraSalida")
    public String outOur;
    @SerializedName("NombreUsuario")
    public String userName;
    @SerializedName("HoraAsistencia")
    public String assistanceHour;
    @SerializedName("TokenG")
    public String googleToken;
}