package com.example.retrofitlogin.view.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MessageUtils {
    private static ProgressDialog sProgressDialog;

    public static void toast(Context context, int message) {
        if (context == null) return;
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void toast(Context context, String message) {
        if (context == null) return;
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void progress(AppCompatActivity appCompatActivityy, String message) {
        stopProgress();
        if (appCompatActivityy != null && !(appCompatActivityy).isFinishing() && !(appCompatActivityy).isDestroyed()) {
            sProgressDialog = ProgressDialog.show(appCompatActivityy, null, message, true, false);
            sProgressDialog.show();
        }
    }

    public static void progress(Activity activity, int message) {
        stopProgress();
        if (activity != null && !(activity).isFinishing() && !(activity).isDestroyed()) {
            sProgressDialog = ProgressDialog.show(activity, null, activity.getString(message), true, false);
            sProgressDialog.show();
        }
    }

    public static void stopProgress() {
        try {
            if (sProgressDialog != null) {
                sProgressDialog.cancel();
                sProgressDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}