package com.example.retrofitlogin.background.definitions;

import com.example.retrofitlogin.modules.assistants.background.request.AssistantsRequest;
import com.example.retrofitlogin.modules.assistants.background.response.AssistantsResponse;
import com.example.retrofitlogin.modules.assistants_added.background.request.AssistantsAddedRequest;
import com.example.retrofitlogin.modules.login.background.request.LoginRequest;
import com.example.retrofitlogin.modules.login.background.response.LoginResponse;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface WebServicesDefinition {
    @POST("/FFMTpe/LoginUserApp")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);

    @POST("/FFMTpe/ConsultaAuxAPP/RestService/process")
    Call<AssistantsResponse> assistants(@Body AssistantsRequest assistantsRequest);

    @POST("/FFMTpe/AgregarAuxiliarAPP")
    Call<AssistantsAddedRequest> addAssistant(@Body AssistantsAddedRequest assistantsAddedRequest);
}
