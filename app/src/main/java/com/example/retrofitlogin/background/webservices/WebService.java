package com.example.retrofitlogin.background.webservices;


import android.util.Base64;
import com.example.retrofitlogin.background.definitions.WebServicesDefinition;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebService {

    public static WebServicesDefinition services () {
        return createChannel().create(WebServicesDefinition.class);
    }

    private static Retrofit createChannel() {

        Retrofit.Builder builder = new Retrofit.Builder();

        builder.baseUrl("https://msstest.totalplay.com.mx:443");
        builder.client(createClient());
        builder.addConverterFactory(GsonConverterFactory.create());

        Retrofit retrofit = builder.build();

        return retrofit;
    }

    private static OkHttpClient createClient() {

        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.readTimeout(120, TimeUnit.SECONDS);
        builder.connectTimeout(120, TimeUnit.SECONDS);

        builder.addInterceptor(chain -> {

            Request.Builder requestBuilder = chain.request().newBuilder();

            requestBuilder.addHeader("Content-Type", "application/json");
            requestBuilder.addHeader("Accept", "application/json");
            requestBuilder.addHeader("Authorization", "Basic " + Base64.encodeToString("ffmapp:4gend4mi3nto".getBytes(), Base64.NO_WRAP));

            return chain.proceed(requestBuilder.build());
        });

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        builder.addInterceptor(interceptor);

        OkHttpClient okHttpClient = builder.build();

        return okHttpClient;
    }

}
