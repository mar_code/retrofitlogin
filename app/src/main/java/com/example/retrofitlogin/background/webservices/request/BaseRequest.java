package com.example.retrofitlogin.background.webservices.request;

import com.example.retrofitlogin.modules.login.background.request.LoginRequest;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseRequest implements Serializable {

    @SerializedName("Login")
    public LoginRequest loginRequest = new LoginRequest();
}
