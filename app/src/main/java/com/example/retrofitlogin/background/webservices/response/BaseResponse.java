package com.example.retrofitlogin.background.webservices.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    @SerializedName(value = "idResult", alternate = {"IdResult", "id_result", "idresult", "idrest"})
    public String idResult;
    @SerializedName(value = "result", alternate = {"res", "Result"})
    public String result;
    @SerializedName(value = "resultDescription", alternate = {"resultDescripcion", "DescripcionR", "ResultDescription", "DescriptionResult", "description", "Description", "resDescription"})
    public String resultDescription;
}
