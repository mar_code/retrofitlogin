package com.example.retrofitlogin;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .name("retrofitlogin")
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }
}
